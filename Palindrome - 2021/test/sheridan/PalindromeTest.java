package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		boolean palindrome = Palindrome.isPalindrome("racecar");

		assertTrue("This string your provided does not match the result.", palindrome);
	}

	@Test
	public void testIsPalindromeNegative() {
		boolean palindrome = Palindrome.isPalindrome("hello");

		assertFalse("This string your provided does not match the result.", palindrome);
	}

	@Test
	public void testIsPalindromeBoundaryIn() {
		boolean palindrome = Palindrome.isPalindrome("Taco cat");

		assertTrue("This string your provided does not match the result.", palindrome);
	}

	@Test
	public void testIsPalindromeBoundaryOut() {
		boolean palindrome = Palindrome.isPalindrome("race on car");

		assertFalse("This string your provided does not match the result.", palindrome);
	}

}
